default:
	just --choose

# Build all the rust
build:
	cargo fmt --all -- --check
	cargo build --release
	exa -lh target/avr-atmega328p/release/*.elf

# Build (debug) all the rust
debug:
	cargo build
	exa -lh target/avr-atmega328p/debug/*.elf

# Run the app natively
run:
	cargo run

# Run the app natively
run-release:
	cargo run --release

# Type-check and lint the code
clippy:
	cargo fmt --all -- --check
	cargo clippy --workspace

# Run Rust tests
test:
	cargo nextest run

# Clean everything
clean:
	cargo clean
