{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    naersk = {
      url = "github:nix-community/naersk";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };
    ravedude = {
      url = "github:Rahix/avr-hal?dir=ravedude";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.naersk.follows = "naersk";
      inputs.utils.follows = "flake-utils";
    };
  };

  outputs = { self, nixpkgs, flake-utils, naersk, rust-overlay, ravedude, ... }:
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        overlays = [ (import rust-overlay) ];
        pkgs = import nixpkgs {
          inherit system overlays;
        };
        rust = (pkgs.rust-bin.nightly.latest.default.override {
          extensions = [ "rust-src" "rust-analyzer" ];
          # targets = [ "wasm32-unknown-unknown" ];
        });
        naersk-lib = naersk.lib."${system}";
        libPath = with pkgs; lib.makeLibraryPath [
        ];
      in
      rec {
        # `nix build`
        packages.app = naersk-lib.buildPackage {
          src = ./.;
        };
        defaultPackage = packages.app;

        # `nix run`
        apps.app = flake-utils.lib.mkApp {
          drv = packages.app;
        };
        defaultApp = apps.app;

        # `nix develop`
        devShell = pkgs.mkShell {
          nativeBuildInputs = with pkgs; [
            cargo-nextest
            cargo-outdated
            difftastic
            exa
            just
            pkg-config
            pre-commit
            rust
            tokei
            usbutils

            avrdude
            pkgsCross.avr.buildPackages.gcc
            ravedude.defaultPackage."${system}"
          ];

          LD_LIBRARY_PATH = libPath;
          GIT_EXTERNAL_DIFF = "${pkgs.difftastic}/bin/difft";
          RAVEDUDE_PORT = "/dev/ttyACM0";
        };
      }
    );
}
